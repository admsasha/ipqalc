#include <math.h>
#include <QStringList>
#include <qregexp.h>

#include "ipadress.h"


IpAdress::IpAdress(QString ipAdress,QString NetMask){
    QString newmask;
    QString tempIP;
    bool ok;


    QStringList tempList = NetMask.split("");
    if (tempList[1] == "/" or NetMask.size()<=2){
        NetMask.replace("/","");
        int prefix = NetMask.toInt(&ok,10);
        for (int i=0;i<prefix;i++) tempIP.append("1");
        for (int i=0;i<(32-prefix);i++) tempIP.append("0");

        for (int x=0;x<4;x++){
            for (int i=(x*8);i<(x*8)+8;i++) newmask.append(tempIP[i]);
            newmask.append(".");
        }        
        this->NetMask = Conv(newmask,2,10);
    }else{
        this->NetMask = NetMask;
    }
    
    this->ipAdress = ipAdress;
    
}

IpAdress::~IpAdress(){

}


QString IpAdress::GetIpAdress(int Base){
    return Conv(ipAdress,10,Base);
}

QString IpAdress::GetNetMask(int Base){
    return Conv(NetMask,10,Base);
}

QString IpAdress::GetInversMask(int Base){
    QStringList mask = this->NetMask.split(".");
    QStringList invmask;
    QString str;

    for (int i=0; i<4; i++){
        invmask.append(str.setNum(((mask[i].toInt()) ^ 255), Base));
    }
    return Conv(invmask.join("."),Base,Base);
    
}

QString IpAdress::GetNetAdress(int Base){



    QStringList adr = this->ipAdress.split(".");
    QStringList mask = this->NetMask.split(".");
    QStringList net;
    QString str;

    for (int i=0; i<4; i++){
        net.append(str.setNum(((adr[i].toInt()) & (mask[i].toInt())), Base));
    }
    return Conv(net.join("."),Base,Base);
}


QString IpAdress::GetMaskPrefix(){
    QStringList binmask = GetNetMask(2).split("");
    int prefix=0;
    
    for (int i=0;i<binmask.count();i++){
	if (binmask[i] == "1") prefix++;
    }

    QString str;
    str.setNum(prefix, 10);
	
    return str;
}

QString IpAdress::GetBroadCast(int Base){
    int prefix = GetMaskPrefix().toInt();
    QString temp=GetIpAdress(2);
    QString broadcast;
    
    temp.replace(".","");
    temp.truncate(prefix);
    for (int i=0;i<(32-prefix);i++){
        temp.append("1");
    }
        
    QStringList tempList = temp.split("");
    for (int x=0;x<4;x++){
	for (int i=(x*8);i<(x*8)+8;i++) broadcast.append(tempList[1+i]);
	broadcast.append(".");
    }
            
    return Conv(broadcast,2,Base);
}

QString IpAdress::GetMinIP(int Base){
    int prefix = GetMaskPrefix().toInt();
    QString temp=GetIpAdress(2);
    QString minip;
    
    temp.replace(".","");
    temp.truncate(prefix);
    for (int i=0;i<(32-prefix-1);i++) temp.append("0");
    temp.append("1");
    
    QStringList tempList = temp.split("");
    for (int x=0;x<4;x++){
	for (int i=(x*8);i<(x*8)+8;i++) minip.append(tempList[1+i]);
	minip.append(".");
    }
            
    return Conv(minip,2,Base);
}

QString IpAdress::GetMaxIP(int Base){
    int prefix = GetMaskPrefix().toInt();
    QString temp=GetIpAdress(2);
    QString maxip;
    
    temp.replace(".","");
    temp.truncate(prefix);
    for (int i=0;i<(32-prefix-1);i++) temp.append("1");
    temp.append("0");
        
    QStringList tempList = temp.split("");
    for (int x=0;x<4;x++){
	for (int i=(x*8);i<(x*8)+8;i++) maxip.append(tempList[1+i]);
	maxip.append(".");
    }
            
    return Conv(maxip,2,Base);
}

QString IpAdress::GetHosts(){
    int hosts;
    QString temp;
    bool ok;
    int prefix = GetMaskPrefix().toInt();

    for (int i=0;i<32-(32-prefix);i++) temp.append("0");
    for (int i=0;i<(32-prefix);i++) temp.append("1");

    hosts=temp.toInt(&ok,2)+1;

    QString str;
    str.setNum(hosts, 10);

    return str;
    
}



QString IpAdress::Conv(QString IpAddr,int FromBase, int ToBase){
    QString str = "";
    QStringList strList;
    QByteArray text;
    int len;
    int MaxLen=0;
    bool ok;

    QStringList adr = IpAddr.split(".");

    for (int i=0;i<4;i++){
        text.setNum(adr[i].toInt(&ok,FromBase),ToBase);
        strList.append(text);
    }
    
    for (int i=0;i<4;i++){
	if (ToBase == 2) MaxLen=8;
	if (ToBase == 16) MaxLen=2;
	//if (ToBase == 10) MaxLen=3;
	
	len=strList[i].length();
	if (len < MaxLen){
	    for (int x=0;x<(MaxLen-len);x++){
	        strList[i].insert(0,"0");
	    }
	     
	}
    }

    str = strList.join(".");
    str = str.toUpper();

    return str;
}


