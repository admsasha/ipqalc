#include <QtGui>
#include <QTranslator>

#include "ipQalc.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QApplication::setApplicationName("IpQalc");
    QApplication::setApplicationVersion(VERSION);
    QApplication::setOrganizationDomain("dansoft.ru");

    QCommandLineParser parser;
    parser.setApplicationDescription(QApplication::tr("Small utility for IP address calculations including broadcast and network addresses as well as Cisco wildcard mask"));
    parser.addVersionOption();
    parser.process(app);

    QTranslator translator;

    QString locale = QLocale::system().name();

    if (translator.load(QString("ipqalc_") + locale)==false){
        translator.load(QString("/usr/share/ipqalc/langs/ipqalc_") + locale);
    }
    app.installTranslator(&translator);


    ipQalc form1;
    form1.show();

    return app.exec();

}
