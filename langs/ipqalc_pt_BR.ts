<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>ipQalc</name>
    <message>
        <source>IP Qalc</source>
        <translation>IP Qalc</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <source>MASK/CIDR</source>
        <translation>MASK/CIDR</translation>
    </message>
    <message>
        <source>calculate</source>
        <translation>Calcular</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <source>Netmask</source>
        <translation>Máscara de rede</translation>
    </message>
    <message>
        <source>Wildcard</source>
        <translation>Máscara inversa</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation>Prefixo</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Rede</translation>
    </message>
    <message>
        <source>Broadcast</source>
        <translation>Broadcast</translation>
    </message>
    <message>
        <source>Min IP</source>
        <translation>IP Mín</translation>
    </message>
    <message>
        <source>Max IP</source>
        <translation>IP Máx</translation>
    </message>
    <message>
        <source>Hosts</source>
        <translation>Hosts</translation>
    </message>
    <message>
        <source>Invalid Input IP address</source>
        <translation>Endereço IP inválido</translation>
    </message>
    <message>
        <source>Invalid Input Netmask.</source>
        <translation type="vanished">Неправильно набрана маска сети.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IPQalc: Script for calculation of subnets&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IPQalc: Script para cálculo de sub-redes&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>00.00.00.00</source>
        <translation>00.00.00.00</translation>
    </message>
</context>
</TS>
