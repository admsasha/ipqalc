#ifndef IPCALC_H
#define IPCALC_H

#include <QtGui>

#include "ui_ipQalc.h"
#include "ipadress.h"

#define VERSION "1.5.3"

class ipQalc : public QWidget {
	Q_OBJECT
    public:
        explicit ipQalc(QWidget *parent = 0);

    private slots:
        void calc();

    private:
        IpAdress *SubNet;
        Ui::ipQalc ui;

        bool isValidIP(QString);
        bool isValidMask(QString);

    protected:
        bool eventFilter(QObject*, QEvent *);
};

#endif
